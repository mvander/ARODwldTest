#!/bin/sh

if [ $# -lt 1 ]; then
echo "Usage: <number of traces to pull e.g. 10>"
exit 1
fi

TRIALS=$1

NUM="0"

while [ $NUM -lt $TRIALS ]
do
scp -r mvander@pc249.emulab.net:~/traces/trace$NUM trace$NUM
NUM=$[$NUM+1]
done
