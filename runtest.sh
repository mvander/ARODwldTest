#!/bin/sh

#  runTest.sh
#  
#
#  Created by Mark Van der Merwe on 7/29/16.
#

if [ $# -lt 2 ]; then
echo "Usage: <adb port # e.g. 8003> <number of tests to run e.g. 10>"
exit 1
fi

UE=$1
TRIALS=$2

RUN="0"

while [ $RUN -lt $TRIALS ]
do
./starttest.py -s pc599.emulab.net:$UE
sleep 5m
./endtest.py -s pc599.emulab.net:$UE

adb pull sdcard/ARO/a ~/trace$RUN
adb shell rm -r -f sdcard/ARO/a

RUN=$[$RUN+1]
echo "Finished run $RUN"
done